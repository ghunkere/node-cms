export default class PluginManager {
    constructor() {
        this.exports = this.load()
        this.plugins = []
    }
    load() {
        require('fs').readdirSync(__dirname + '/../plugins').forEach((file) => {
            if (file.match(/\.js$/) !== null && file !== 'index.js') {
                var name = file.replace('.js', '');
                exports[name] = require('./../plugins/' + file);
            }
        });

        return Object.keys(exports).map(x => {
            if (exports[x].default != undefined) return exports[x].default
        }).filter(x => x != undefined)
    }
    async initialize(app, db) {
        let pluginsList = (await db.collection('plugins').find({}).toArray()).map(x => x.name);

        await Promise.all(this.exports.map(async plugin => {
            let pl = new plugin(app, db)
            if (pluginsList[pl.name] == undefined) {
                await pl.install()
            }
            this.plugins.push(pl)
            if (pl.enabled)
                pl.initMethods()
        }))

        console.log(this.plugins.map(x => x.name))

    }
    async before(req, res) {
        await Promise.all(this.plugins.filter(pl => pl.enabled).map(async pl => await pl.before(req, res)))
    }
    async after(req, res, reponseObject) {
        await Promise.all(this.plugins.filter(pl => pl.enabled).map(async pl => await pl.after(req, res, reponseObject)))
        res.send(reponseObject)
    }
}
