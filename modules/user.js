
import Model from "../src/model";
import Utils from "../src/utils";
export class UserMethods {
    constructor(app, db) {
        this.db = db
        this.app = app
        this.errorCatcher = (fn) => (req, res, next) => Promise.resolve(fn(req, res, next, db)).catch(next);
        this.initMethods()
    }
    initMethods() {
        this.app.post("/auth", this.errorCatcher(this.auth));
    }
    async auth(req, res, next, db) {
        let mToken = new Model(db, "tokens");
        let mUser = new Model(db, "users");

        let filter = {
            login: req.body.login,
            password: req.body.password
        };
        let user = await mUser.fetch(filter);
        if (user.length == 0 || (req.body.login == undefined || req.body.password == undefined)) {
            throw {
                status: 401,
                message: "Password or login are incorrect"
            };
        }
        let token = {
            token: Utils.guid(),
            user_id: user[0].id,
            csrf: Utils.guid(),
            expired_at: Date.now() + 1000 * 60 * 60 * 24
        };
        res.set('csrf-token', token.csrf);
        res.send(await mToken.insert(token))
        // res.send(await mToken.insert(token));
    }

    async register() {

    }

    async changePass() {

    }

}
