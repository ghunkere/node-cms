
import Model from "../src/model";
import Utils from "../src/utils";

export class Schemas {
    constructor(app, db) {
        this.db = db
        this.app = app
        this.errorCatcher = (fn) => (req, res, next) => Promise.resolve(fn(req, res, next, db)).catch(next);
        this.initMethods()
    }
    initMethods() {
        this.app.post("/schema-edit", this.errorCatcher(this.edit));
        this.app.post("/schema-new", this.errorCatcher(this.create));
        this.app.post("/schema-delete", this.errorCatcher(this.delete));
    }

    //How to edit
    // let edit = {
    //     permissions: {
    //         permissions: {
    //             type: 'number',
    //             ref: {
    //                 table: 'access_control',
    //                 local_key: 'id',
    //                 foreign_key: 'role_id',
    //                 property: 'permissions'
    //             }
    //         }
    //     }
    // }

    async edit(req, res, next, db) {
        let schema = await this.getSchema(req)


        let edit = req.body.edit
        let deleted = req.body.delete
        let newFields = req.body.new
        let required = req.body.required

        if (required != undefined) {
            var notFoundProps = required.filter(prop => schema[prop] == undefined)
            if (notFoundProps.length > 0)
                throw {
                    status: 400,
                    message: $`Required fields [${notFoundProps.map(x => x + ",")}] not found in ` + schema.model
                };

            schema.required = required
        }


        Object.keys(newFields).map(key => {
            if (newFields[key]['default'] == undefined && newFields[key]['nullable'] == undefined)
                throw {
                    status: 400,
                    message: $`New fields [${newFields[key]}] must be nullable or with default value`,
                }
        })

        Object.keys(edit).map(key => schema[key] != undefined ? schema[key] = edit[key] : schema[key])
        Object.keys(newFields).map(key => schema[key] = newFields[key])
        Object.keys(deleted).map(key => delete schema[key])


        var rename = {}
        var unset = {}
        var set = {}
        
        Object.keys(edit).map(key => edit[key][key] == undefined ? rename[key] = edit[key][key] : "")
        Object.keys(deleted).map(key => unset[key] = 1)
        Object.keys(newFields).map(key => set[key] = newFields[key].default)


        await db.collection(schema.model).update({}, {
            $rename: rename,
            $set: set,
            $unset: unset
        }, { multi: true });

        res.send(await schema.update(req.body.filter, schema))
    }

    async create(req, res, next, db) {
        let schema = await this.getSchema(req)
        let model = req.body.model
        let required = model.required

        if (required != undefined) {
            var notFoundProps = required.filter(prop => schema[prop] == undefined)
            if (notFoundProps.length > 0)
                throw {
                    status: 400,
                    message: $`Required fields ${notFoundProps.toString()} not found in ` + schema.model
                };

            schema.required = required
        }

        if (Object.keys(model.schema).length == 0)
            throw {
                status: 400,
                message: 'Schema props not found'
            };

        let types = ['string', 'number', 'boolean']
        let error = Object.keys(model.schema).filter(key => {
            if (model.schema[key].type == undefined || types.indexOf(model.schema[key].type) == -1)
                return true
        })
        if (error.length > 0)
            throw {
                status: 400,
                message: $`Incorrect types ${error.map(key => model.schema[key].type).toString()} must be ${types.toString()}`
            };

        res.send(await schema.insert(req.body.filter, req.body.model))
    }

    async delete(req, res, next, db) {
        let schema = await this.getSchema(req)
        res.send(await schema.delete(req.body.filter))
    }

    async getSchema(req) {
        let mSchema = new Model(db, "schemas");
        let filter = {
            model: req.body.filter.name
        };
        let schema = await mSchema.fetch(filter);

        if (schema.length == 0 || req.body.filter.name == undefined) {
            throw {
                status: 404,
                message: "Schema not found"
            };
        }
        return schema;
    }
}
