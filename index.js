const express = require("express");
const mongoClient = require("mongodb").MongoClient;
const bodyParser = require("body-parser");
const helmet = require('helmet');

import RBAC from "./src/RBAC";
import Model from "./src/model";
import Init from "./src/init";
import { UserMethods } from "./modules/user";
import PluginManager from "./modules/pluginManager";


require("babel-polyfill");

var app = express();
var db = null;

var pm = new PluginManager()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(helmet());

app.use("/:collection", (req, res, next) => {
  if (req.params.collection == undefined) res.status(404).send("Collection not found");
  next();
});


const errorCatcher = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};

app.post("/:collection/create", errorCatcher(async (req, res) => {
  await pm.before(req, res)
  let model = new Model(db, req.params.collection);
  await pm.after(req, res, await model.insert(req.body, req.query.token))
}));

app.post("/:collection/read", errorCatcher(async (req, res) => {
  await pm.before(req, res)
  let model = new Model(db, req.params.collection);
  await pm.after(req, res, await model.fetch(req.body), req, res)
}));

app.post("/:collection/update", errorCatcher(async (req, res) => {
  await pm.before(req, res)
  let model = new Model(db, req.params.collection);
  await pm.after(req, res, await model.update(req.body.filter, req.body.update));
}));

app.post("/:collection/delete", errorCatcher(async (req, res) => {
  await pm.before(req, res)
  let model = new Model(db, req.params.collection);
  await pm.after(req, res, await model.delete(req.body));
}));



mongoClient.connect("mongodb://91.239.26.200:27017/", async (err, client) => {
  if (err) return
  db = client.db("test");
  new Init(db);
  new RBAC(app, db);
  new UserMethods(app, db)
  await pm.initialize(app, db)
  // Catching thrown errors. Middleware must be last
  app.use(errorHandler);


  app.listen(3000, () => {
    console.log("listening on http://localhost:3000/");
  });
});





function errorHandler(err, req, res, next) {
  console.log("THROWED", err);
  if (err.status != undefined) res.status(err.status).send(err);
  else res.status(500).send({
    status: 500,
    message: "Unexpected error"
  });
}