import Model from '../src/model'
export default class Likes {
    constructor(app, db) {
        this.app = app;
        this.db = db;
        this.name = "Likes"
        this.enabled = true
        this.collection = "likes"
        this.description = "Add likes for every object in system"
        this.errorCatcher = (fn) => (req, res, next) => Promise.resolve(fn(req, res, next, db)).catch(next);
    }

    before(req, res) {
        console.log('before')
    }
    after(req, res, object) {
        // let root = object.findIndex(x => x.login == 'root')
        // if (root > -1)
        //     object.splice(root)

    }
    async install() {
        await this.db.collection('schemas').remove({ model: this.collection })
        await this.db.collection('collections').remove({ name: this.collection })
        await this.db.collection('plugins').remove({ name: this.name })
        await this.db.collection('plugins_joins').remove({ plugin_collection: this.collection })
        await this.db.collection('schemas').insert({
            model: this.collection,
            required: ['collection', 'item_id', 'user_id'],
            schema: {
                collection: {
                    type: 'string'
                },
                item_id: {
                    type: 'number'
                },
                user_id: {
                    type: 'number',
                    ref: {
                        table: 'users',
                        local_key: 'item_id',
                        foreign_key: 'id',
                        property: 'user',
                        props: {
                            'user.password': 0
                        }
                    }
                },
            }
        })
        let coll = new Model(this.db, 'collections')
        await coll.insert({ name: this.collection })

        let plugin = new Model(this.db, 'plugins')
        let result = await plugin.insert({
            name: this.name,
            description: this.description,
            collection: this.collection,
            enabled: true
        })

        let pluginJoins = new Model(this.db, 'plugins_joins')
        await pluginJoins.insert({
            plugin_id: result.id,
            plugin_collection: this.collection,
            collection: 'users',
            plugin_key: 'item_id',
            collection_key: 'id',
            response_type: "count",
            property_name: "_likes"
        })
    }

    initMethods() {
        this.app.post('/:collection/like', this.errorCatcher(this.like))
    }
    async like(req, res, next, db) {
        let token = await db.collection('tokens').findOne({ token: req.query.token })
        req.body['user_id'] = token.user_id

        let mLike = new Model(db, 'likes')
        let like = (await mLike.fetch({ filter: req.body }))[0]
        console.log(like, { filter: req.body })
        if (like != undefined) {
            res.send(like)
        }
        else {
            let body = {
                collection: req.params.collection,
                item_id: req.body.item_id,
                user_id: token.user_id
            }
            res.send(await mLike.insert(body, req.query.token))

        }

    }

}