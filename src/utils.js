export default {
    guid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    treeView(parentItems, fullList, key, foreignKey, firstRun = true) {
        let list = [];
        if (firstRun)
            parentItems = fullList.filter(item => item[key] == null)
   
        parentItems.map(pItem => {
            let childs = fullList.filter(item => pItem[foreignKey] == item[key]);
            pItem["childs"] = this.treeView(childs, fullList, key, foreignKey, false);
            list.push(pItem);
        });
        return list;
    }
}