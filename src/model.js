require('babel-polyfill');
import utils from './utils';


var ObjectId = require('mongodb').ObjectID;
export default class model {
  constructor(db, collectionName) {
    this.db = db;
    this.collectionName = collectionName;
    this.dbModel = null;
  }
  async findScheme() {
    this.dbModel = await this.db
      .collection('schemas')
      .findOne({ model: this.collectionName });
    if (this.dbModel == null)
      throw {
        status: 404,
        message: 'Scheme not found'
      };
  }

  async validate(body, operation) {
    if (this.dbModel == null) await this.findScheme();
    //Check fields
    if (operation == 'create') {
      this.dbModel.required.map(field => {
        if (body[field] == undefined)
          throw {
            status: 400,
            message: field + ' is required'
          };
      });
    } else {
      Object.keys(body).map(field => {
        if (this.dbModel.schema[field] == undefined) delete body[field];
      });
    }
    //check types
    Object.keys(this.dbModel.schema).map(field => {

      if (typeof body[field] != this.dbModel.schema[field].type && !this.dbModel.schema[field].nullable && !_.isNull(body[field])) {
        throw {
          status: 400,
          message: field + ' incorrect type, must be ' + this.dbModel.schema[field].type
        };
      }
    });

    return true;
  }
  async fetch(body) {
    if (this.dbModel == null) await this.findScheme();

    let dbModel = this.dbModel.schema;
    let params = [
      { $match: body.filter == undefined ? {} : body.filter },

    ];
    
    if (body._pagination != undefined) {
      let count = body._pagination.count == undefined ? 10 : body._pagination.count
      let skip = body._pagination.page == undefined ? 0 : count * body._pagination.page

      param.push({ $skip: skip })
      param.push({ $limit: count })
    }

    Object.keys(dbModel).map(x => {
      //Hide hidden fields
      if (dbModel[x].hidden) {
        params.push({ $project: { [x]: 0 } })
      }
      // work with joined collections
      if (dbModel[x].ref != undefined) {
        params.push({
          $lookup: {
            from: dbModel[x].ref.table,
            localField: dbModel[x].ref.local_key,
            foreignField: dbModel[x].ref.foreign_key,
            as: dbModel[x].ref.property,
          },
        });
        //Filter showned fields by props values
        if (dbModel[x].ref.props != undefined) {
          params.push({ $project: dbModel[x].ref.props })
        }

        //Pagination for joined collection
        params.push({
          $addFields: {
            [dbModel[x].ref.property]: { $slice: ['$' + dbModel[x].ref.property, 10] }
          }
        })


      }
    });
    //Join referenced collection
    let relativePluginJoin = await this.db.collection('plugins_joins').findOne({ collection: this.collectionName })
    if (relativePluginJoin) {
      params.push({
        $lookup: {
          from: relativePluginJoin.plugin_collection,
          localField: relativePluginJoin.collection_key,
          foreignField: relativePluginJoin.plugin_key,
          as: relativePluginJoin.property_name,
        },
      });
      // return count of object instead objects
      if (relativePluginJoin.response_type == "count") {
        params.push({
          $addFields: {
            [relativePluginJoin.property_name]: { $size: '$' + relativePluginJoin.property_name }
          }
        })
      } else
        params.push({
          $addFields: {
            [relativePluginJoin.property_name]: { $slice: ['$' + relativePluginJoin.property_name, 10] }
          }
        })
    }

    let result = await this.db
      .collection(this.collectionName)
      .aggregate(params)
      .toArray()

    //Use tree view for responsed object
    if (body.treeView != undefined)
      result = utils.treeView([], result, body.treeView.key, body.treeView.foreignKey)

    return result
  }

  async insert(body, qToken) {
    //Get owner by token or default root user
    let token = await this.db.collection('tokens').findOne({ token: qToken })

    await this.checkAndAddToCollectionsList();
    await this.validate(body, 'new');
    await this.setDefaultValues(body, token == null ? null : token.user_id);
    let inserted = await this.db.collection(this.collectionName).insertOne(body);
    let entity = await this.db.collection(this.collectionName).findOne({ _id: ObjectId(inserted.insertedId) });
    return entity;
  }

  async update(filter, body) {
    this.validate(body, 'update');
    delete body['id'];

    body['updated_at'] = Date.now();
    let updated = await this.db.collection(this.collectionName).update(filter, { $set: body }, { returnNewDocument: true, multi: true });
    return updated;
  }



  async delete(filter) {
    let deleted = await this.db.collection(this.collectionName).findOneAndDelete(filter);
    return deleted;
  }

  async setDefaultValues(body, owner_id) {
    body['id'] = await this._autoincrement(this.collectionName);
    let dbModel = this.dbModel.schema;
    Object.keys(dbModel).map(x => {
      if (body[x] == undefined && dbModel[x].default != undefined) {
        body[x] = dbModel[x].default;
      }
    });
    body['owner_id'] = owner_id;
    body['created_at'] = Date.now();
  }
  async checkAndAddToCollectionsList() {
    if (this.collectionName != 'collections')
      await this.db.collection('collections').update(
        { name: this.collectionName },
        {},
        { upsert: true }
      );
  }
  async _autoincrement(name) {
    var ret = await this.db
      .collection('counters')
      .findAndModify(
        { name: name },
        [],
        { $inc: { id: 1 } },
        { upsert: true, new: true }
      );
    return ret.value.id;
  }
}
