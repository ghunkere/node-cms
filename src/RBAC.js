import utils from "./utils";

export default class RBAC {
  constructor(app, db) {
    this.app = app;
    this.db = db;
    // this.init();
  }
  init() {
    this.app.use('/:collection', async (req, res, next) => {
      let qToken = req.query.token;
      if (req.originalUrl == '/auth') {
        next();
        return;
      }

      if (qToken == '' || qToken == undefined) {
        res.status(401).send({
          status: 401,
          message: 'Token expired or not exist'
        });
        return;
      }

      let token = await this.db.collection('tokens').findOne({ token: qToken });
      if (token != null && token.expired_at > Date.now()) {
        //Check csrf token and update token
        let csrf = req.get('csrf-token')

        if (csrf != token.csrf) {
          res.status(403).send({
            status: 403,
            message: 'Invalid csrf token'
          });
          return
        }

        csrf = utils.guid()
        res.set('csrf-token', csrf)
        await this.db.collection('tokens').findOneAndUpdate({ token: qToken }, { $set: { csrf: csrf } });

        //Get user access by token
        let user = await this.db.collection('users').findOne({ id: token.user_id });
        let access = await this.db.collection('access_control').findOne({
          $and: [
            { role_id: user.role_id },
            {
              $or: [{ collection: req.params.collection }, { collection: '' }]
            }
          ]
        });

        if (access == null) {
          res.status(403).send({
            status: 403,
            message: 'Access denide'
          });
          return;
        }

        let action = req.url.substring(1, req.url.indexOf('token')).replace('?', '');
        let permissions = access.permissions.split('');
        let table = { get: 0, new: 1, update: 2, detete: 3 };

        if (permissions[table[action]] == 0) {
          res.status(403).send({
            status: 403,
            message: 'Access denide'
          });
          return;
        }
        if (permissions[table[action]] == 2 && action != "insert") {
          body.filter['owner_id'] = token.user_id
        }

        next();

      } else
        res.status(401).send({
          status: 401,
          message: 'Token expired or not exist'
        });
    });


  }
}
