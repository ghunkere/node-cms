import Model from './model.js';
export default class init {
  constructor(db) {
    this.db = db;
    this.createModels();
  }
  async createIndexes() {
    this.db.collection('collection').createIndex({ name: 1 })
    this.db.collection('schemas').createIndex({ model: 1 })
    this.db.collection('users').createIndex({ user_id: 1, login: 1, password: 1 })
    this.db.collection('tokens').createIndex({ token: 1, csrf: 1 })
    this.db.collection('access_control').createIndex({ role_id: 1, collection: 1 })
    this.db.collection('plugins').createIndex({ name: 1, collection: 1 })
    this.db.collection('plugins_joins').createIndex({ plugin_id: 1, collection: 1 })
  }
  async fillCollections() {
    let coll = new Model(this.db, 'collections');
    let promises = [
      coll.insert({ name: 'schemas' }),
      coll.insert({ name: 'tokens' }),
      coll.insert({ name: 'users' }),
      coll.insert({ name: 'access_control' }),
      coll.insert({ name: 'collections' })
    ];
    await Promise.all(promises);
    await this.createIndexes()
  }
  createModels() {
    this.db.collection('schemas').drop();
    this.db.collection('collections').drop();
    this.db.collection('schemas').insertMany(
      [
        require('../schemas/tokens.js'),
        require('../schemas/users.js'),
        require('../schemas/access_control.js'),
        require('../schemas/roles.js'),
        require('../schemas/plugin_joins.js'),
        require('../schemas/collections.js'),
        require('../schemas/plugins.js'),
        require('../schemas/menu.js')
      ],
      async (e, r) => {
        await this.fillCollections();
      }
    );
  }
}
