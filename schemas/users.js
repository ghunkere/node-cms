module.exports = {
    model: 'users',
    required: ['login', 'password'],
    schema: {
        login: {
            type: 'string'
        },
        password: {
            type: 'string',
            hidden: true,
        },
        role_id: {
            type: 'number',
            default: 1,
            ref: {
                table: 'roles',
                local_key: 'role_id',
                foreign_key: 'id',
                property: 'role'
            }
        }
    }
}