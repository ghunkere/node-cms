module.exports = {
    model: 'plugins',
    required: ['name', 'collection'],
    schema: {
        name: {
            type: 'string'
        },
        description: {
            type: 'string'
        },
        collection: {
            type: 'string'
        },
        enabled: {
            type: 'boolean',
            default: false,
        }
    }
}