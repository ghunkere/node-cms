module.exports = {
    model: 'menu',
    required: ['name', 'route'],
    schema: {
        name: {
            type: 'string'
        },
        route: {
            type: 'string'
        },
        order_id: {
            type: 'number',
        },
        pid: {
            type: 'number',
            nullable: true
            // ref: {
            //     table: 'users',
            //     local_key: 'user_id',
            //     foreign_key: 'id',
            //     property: 'user',
            //     props: {
            //         'user.password': 0
            //     }
            // }
        },
    }
}