module.exports = {
    model: 'tokens',
    required: ['token', 'user_id', 'expired_at'],
    schema: {
        token: {
            type: 'string'
        },
        csrf: {
            type: 'string'
        },
        user_id: {
            type: 'number',
            ref: {
                table: 'users',
                local_key: 'user_id',
                foreign_key: 'id',
                property: 'user',
                props: {
                    'user.password': 0
                }
            }
        },
        expired_at: {
            type: 'number'
        }
    }
}