module.exports = {
    model: 'plugins_joins',
    required: ['plugin_id', 'plugin_collection', 'collection', 'plugin_key', 'collection_key'],
    schema: {
        plugin_id: {
            type: 'number',
            ref: {
                table: 'plugins',
                local_key: 'plugin_id',
                foreign_key: 'id',
                property: 'plugin'
            }
        },
        plugin_collection: {
            type: 'string'
        },
        collection: {
            type: 'string'
        },
        plugin_key: {
            type: 'string'
        },
        collection_key: {
            type: 'string'
        },
        response_type: {
            type: 'string'
        },
        property_name: {
            type: 'string'
        },
    }
}