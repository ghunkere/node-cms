module.exports = {
    model: 'roles',
    required: ['name'],
    schema: {
        name: {
            type: 'string'
        },
        permissions: {
            type: 'number',
            ref: {
                table: 'access_control',
                local_key: 'id',
                foreign_key: 'role_id',
                property: 'permissions'
            }
        }
    }
}
