module.exports = {
    model: 'collections',
    required: ['name'],
    schema: {
      name: {
        type: 'string'
      }
    }
  }