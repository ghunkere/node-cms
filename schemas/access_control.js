module.exports = {
    model: 'access_control',
    required: ['collection', 'role_id'],
    schema: {
        collection: {
            type: 'string'
        },
        role_id: {
            type: 'number',
            ref: {
                table: 'roles',
                local_key: 'role_id',
                foreign_key: 'id',
                property: 'role'
            }
        },
        permissions: {
            type: 'string',
            default: '1000'
        }
    }
}